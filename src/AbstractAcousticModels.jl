module AbstractAcousticModels

export AbstractAcousticModel,
    loglikelihood,
    numpdfs,
    params,
    update!


###############################################################################
# Abstract AcousticModel type

"""
    abstract type AbstractAcousticModel end

Abstract type for an acoustic model.
"""
abstract type AbstractAcousticModel end

###############################################################################
# Abstract Acoustic Model functions

""""
    loglikelihood(model, x, fs)

Compute likelihood for a given signal
Input :
- model : Acoustic model struture that inherits from AbstractAM
- x : signal (Vector{T})
- fs : sampling rate
Returns matrix of tokens / scores
"""
loglikelihood(model, x, fs) = throw("Not implemented")

"""
    params(model)

Returns model parameters from an acoustic model
"""
params(model) = throw("Not implemented")


"""
    numpdfs(model)

Returns number of emission densities (or analog)
"""
numpdfs(model) = throw("Not implemented")


"""
    update!(model, grad)
(Update weights)
"""
update!(model, grad) = throw("Not implemented")

end
