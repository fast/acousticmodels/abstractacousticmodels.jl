# AbstractAcousticModels.jl

*AbstractAcousticModels* is a Julia package describing the functions to
implement for an acoustic model to be integrated in FAST speech pipelines.

## Installation

This package is part of the [FAST](https://gitlab.lisn.upsaclay.fr/fast) tool
collection and requires the  [FAST registry](https://gitlab.lisn.upsaclay.fr/fast/registry)
to be installed. To install the registry type:
```
pkg> add registry "https://gitlab.lisn.upsaclay.fr/fast/registry"
```
in the package mode of the Julia REPL. You can then install
AbstractAcousticModels.jl package with the command:
```julia
pkg> add AbstractAcousticModels
```

